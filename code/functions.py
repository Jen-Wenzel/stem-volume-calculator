# Wenzel, Jennifer
import os
import math
from rpy2 import robjects
from rpy2.robjects.packages import importr

# import TapeR functions
taper = importr('TapeR')
r_source = robjects.r['source']
r_source('taper.R')
get_d = robjects.globalenv['get_mid_d']
spline_vol = robjects.globalenv['spline_vol']

# set own paths to R installation, in case it is not found
#os.environ["R_HOME"] = r"C:\Program Files\R\R-4.1.1"
#os.environ["PATH"]   = r"C:\Program Files\R\R-4.1.1\bin\x64" + ";" + os.environ["PATH"]


# util functions

# convert cm to m
def cm_to_m(l):
    return l/100

# convert cm to dm
def cm_to_dm(l):
    return l/10

# calculate basal area in m
def calc_ba(d):
    d = cm_to_m(d)
    ba = math.pi * (d/2)**2
    return ba 

# get estimated mid diameter according to taper curve
def get_mid_d(dbh, h):
    result = robjects.FloatVector(get_d(dbh,h))
    return(result[0])

# volume functions

# calculate volume with Huber method
def huber(params):
    species, dbh, h = params
    vols = []
    for s, d, h in zip(species, dbh, h):
        if s == 'Picea abies':
            mid_d = get_mid_d(d, h)
            ba = calc_ba(mid_d)
        else:
            ba = calc_ba(d)
        vol = ba * h
        vols.append(round(vol, 3))
        
    return vols

# dict of Denzin parameters
const_denzin = {'Picea': {'nh': '19+2*dbh_dm', 'vc': 0.04},
                'Pinus': {'nh': 28, 'vc': 0.03}, 
                'Fagus': {'nh': 25, 'vc': 0.03},
                'Quercus': {'nh': 24, 'vc': 0.03},
                'Betula': {'nh': 31, 'vc': 0.03}}

# gets the correct normal height for respective species
def get_nh(species, dbh):
    genus = species.split()[0]
    if genus != 'Picea':
        nh = const_denzin[genus]['nh']
    else:
        dbh_dm = cm_to_dm(dbh)
        nh = const_denzin[genus]['nh']
        nh = eval(nh)
    return nh

# calculate volume with Denzin method
def denzin(params):
    species, dbh, h = params

    try:
        # get correct constants for species
        nh = [get_nh(s, d) for s, d in zip(species, dbh)]
        vc = species.apply(lambda s: const_denzin[s.split()[0]]['vc'])
    
        vol = (dbh**2/1000) + (dbh**2/1000) * (h-nh) * vc
        return round(vol, 3)
    except KeyError:
        return None

# calculate form factor based on quotient of dsm_vol and huber_vol columns
def form_factor(real_vol, cylinder_vol):
    return round(real_vol/cylinder_vol, 3)

# calculate volume with spline method
def spline(params):
    species, dbh, h = params
    vols = []
    for s, d, h in zip(species, dbh, h):
        if s == 'Picea abies':
            vol = spline_vol(d,h)
            vols.append(round(vol[0], 3))
        else:
            vols.append(None)
    return vols