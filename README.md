# Programming 1 

<img src="img/ascii_tree.PNG" alt="Intro" width="50%"/>

&nbsp;

### Setup

1. Create virtual environment 

```
python -m venv venv
```

2. Install requirements

```
pip install -r requirements.txt
```

3. Activate virtual environment

```
venv/Scripts/Activate
```

**Notes:** *Commands may vary depending on the operating system. Rpy2 package requires working installation of R.*

  
4. Run main.py file

```
python code/main.py
```

### Calculate volumes

1. Select sample file from the input folder or choose your own file path.

2. Select method that should be used to calculate the volume.

Note: the attribute names have to match the specifications of this program -> see data model.